package TtrabajoIndividialProcesadorDeTexto;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class Main {

    public static void main(String[] args) {
        new VentanaPrincipal();
    }

    static class VentanaPrincipal extends javax.swing.JFrame {
        public VentanaPrincipal() {
            setSize(550,420);
            setLocationRelativeTo(null);
            PanelPrincipal Ventana = new PanelPrincipal();
            setTitle("Procesador de Texto");
            add(Ventana);
            setVisible(true);
            addWindowListener(new onClose());
        }

        class onClose extends java.awt.event.WindowAdapter {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                super.windowClosing(e);
                try {
                    ObjectOutputStream Guardar = new ObjectOutputStream(new FileOutputStream(PanelPrincipal.getDireccion(false)));
                    Guardar.writeObject(PanelPrincipal.getTexto().getText());
                    Guardar.writeObject(PanelPrincipal.getTexto().getStyledDocument());
                }
                catch (Exception E) {
                    System.out.println("Error al guardar");
                }
                dispose();
            }
        }
    }

}