package TtrabajoIndividialProcesadorDeTexto;

import javax.swing.*;
import javax.swing.text.StyledEditorKit;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;

public class PanelPrincipal extends JPanel {

    private JScrollPane Scroll = new JScrollPane();

    private static JTextPane Texto = new JTextPane();

    private JMenu Fuente = new JMenu("Fuente");
    private JMenu Source = new JMenu("Source");
    private JMenuItem Code = new JMenuItem("Source Code Pro"), Sans = new JMenuItem("Source Sans Pro"), Serif = new JMenuItem("Source Serif Pro");
    private JMenu Segoe = new JMenu("Segoe");
    private JMenuItem Print = new JMenuItem("Segoe Print"), Script = new JMenuItem("Segoe Script"), UI = new JMenuItem("Segoe UI");
    private JMenu MS = new JMenu("MS");
    private JMenuItem Gotic = new JMenuItem("MS Gotic"), PMincho = new JMenuItem("MS PMincho"), SansSerif = new JMenuItem("MS Reference Sans Serif");

    private JMenu Estilo = new JMenu("Estilo");
    private JMenuItem Negrita = new JMenuItem("Negrita");
    private JMenuItem Cursiva = new JMenuItem("Cursiva");

    private JMenu Tamanio = new JMenu("Tamanio");
    private JMenu MenuTamanio1 = new JMenu("3 a 8");
    private JMenuItem T3 = new JMenuItem("3"), T5 = new JMenuItem("5"), T8 = new JMenuItem("8");
    private JMenu MenuTamanio2 = new JMenu("10 y 12");
    private JMenuItem T10 = new JMenuItem("10"), T12 = new JMenuItem("12");
    private JMenu MenuTamanio3 = new JMenu("14 y 18");
    private JMenuItem T14 = new JMenuItem("14"), T18 = new JMenuItem("18");
    private JMenu MenuTamanio4 = new JMenu("24 y 36");
    private JMenuItem T24 = new JMenuItem("24"), T36 = new JMenuItem("36");
    private JMenu MenuTamanio5 = new JMenu("48 y 72");
    private JMenuItem T72 = new JMenuItem("72"), T48 = new JMenuItem("48");

    private JMenu Archivo = new JMenu("Archivo");
    private JMenu Abrir = new JMenu("Abrir");
    private static JFileChooser DireccionArchivo = new JFileChooser();
    private JButton BotonAbrir = new JButton("Abrir");
    private JMenu Guardar = new JMenu("Guardar");
    private JButton BotonGuardar = new JButton("Guardar");
    private static JFileChooser DireccionGuardado = new JFileChooser();

    public PanelPrincipal() {

        setLayout(new BorderLayout());
        JMenuBar Menu = new JMenuBar();

        //--------------------------------------------------------

        Tamanio.add(MenuTamanio1);
        MenuTamanio1.add(T3);
        MenuTamanio1.add(T5);
        MenuTamanio1.add(T8);

        Tamanio.add(MenuTamanio2);
        MenuTamanio2.add(T10);
        MenuTamanio2.add(T12);

        Tamanio.add(MenuTamanio3);
        MenuTamanio3.add(T14);
        MenuTamanio3.add(T18);

        Tamanio.add(MenuTamanio4);
        MenuTamanio4.add(T24);
        MenuTamanio4.add(T36);

        Tamanio.add(MenuTamanio5);
        MenuTamanio5.add(T48);
        MenuTamanio5.add(T72);

        Fuente.add(Source);
        Source.add(Code);
        Source.add(Sans);
        Source.add(Serif);

        Fuente.add(Segoe);
        Segoe.add(Print);
        Segoe.add(Script);
        Segoe.add(UI);

        Fuente.add(MS);
        MS.add(Gotic);
        MS.add(PMincho);
        MS.add(SansSerif);

        Archivo.add(Abrir);
        Abrir.add(DireccionArchivo);
        Abrir.add(BotonAbrir);

        Archivo.add(Guardar);
        Guardar.add(DireccionGuardado);
        Guardar.add(BotonGuardar);

        Estilo.add(Negrita);
        Estilo.add(Cursiva);

        //--------------------------------------------------------

        T3.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 3));
        T5.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 5));
        T8.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 8));
        T10.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 10));
        T12.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 12));
        T14.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 14));
        T18.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 18));
        T24.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 24));
        T36.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 36));
        T48.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 48));
        T72.addActionListener(new StyledEditorKit.FontSizeAction("Resize", 72));

        Code.addActionListener(new StyledEditorKit.FontFamilyAction("", "Source Code Pro"));
        Sans.addActionListener(new StyledEditorKit.FontFamilyAction("", "Source Sans Pro"));
        Serif.addActionListener(new StyledEditorKit.FontFamilyAction("", "Source Serif Pro"));
        Print.addActionListener(new StyledEditorKit.FontFamilyAction("", "Segoe Print"));
        Script.addActionListener(new StyledEditorKit.FontFamilyAction("", "Segoe Script"));
        UI.addActionListener(new StyledEditorKit.FontFamilyAction("", "Segoe UI"));
        Gotic.addActionListener(new StyledEditorKit.FontFamilyAction("", "MS Gotic"));
        PMincho.addActionListener(new StyledEditorKit.FontFamilyAction("", "MS PMincho"));
        SansSerif.addActionListener(new StyledEditorKit.FontFamilyAction("", "MS Sans Serif"));

        Negrita.addActionListener(new StyledEditorKit.BoldAction());
        Negrita.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
        Cursiva.addActionListener(new StyledEditorKit.ItalicAction());
        Cursiva.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));

        BotonAbrir.addActionListener(new AccionArchivo.Guardar());
        DireccionArchivo.setControlButtonsAreShown(false);
        BotonGuardar.addActionListener(new AccionArchivo.Abrir());
        DireccionGuardado.setControlButtonsAreShown(false);

        //-----------------------------------------------------------

        Menu.add(Archivo);
        Menu.add(Fuente);
        Menu.add(Estilo);
        Menu.add(Tamanio);

        //------------------------------------------------------------

        add(Menu, BorderLayout.NORTH);
        Scroll.setLayout(new ScrollPaneLayout());
        Scroll.setViewportView(Texto);
        add(Scroll, BorderLayout.CENTER);
    }

    public static JTextPane getTexto() {
        return Texto;
    }

    public static void setTexto(String texto, javax.swing.text.StyledDocument estilos) {
        Texto.setText(texto);
        Texto.setStyledDocument(estilos);
    }

    public static void setDireccion(File Direccion) {
        DireccionArchivo.setSelectedFile(Direccion);
        DireccionGuardado.setSelectedFile(Direccion);
    }

    public static File getDireccion(boolean Abrir) {
        if(Abrir) return DireccionArchivo.getSelectedFile();
        else return DireccionGuardado.getSelectedFile();
    }

}