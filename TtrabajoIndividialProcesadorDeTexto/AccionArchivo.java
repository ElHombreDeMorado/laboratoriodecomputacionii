package TtrabajoIndividialProcesadorDeTexto;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class AccionArchivo {

    static class Guardar implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                ObjectInputStream Abrir = new ObjectInputStream(new FileInputStream(PanelPrincipal.getDireccion(true)));
                PanelPrincipal.setTexto((String) Abrir.readObject(), (javax.swing.text.StyledDocument) Abrir.readObject());
                PanelPrincipal.setDireccion(PanelPrincipal.getDireccion(true));
            } catch (Exception E) {
                new VentanaMensajeError("Archivo inexistente o no seleccionado");
            }
        }
    }
    static class Abrir implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                ObjectOutputStream Guardar = new ObjectOutputStream(new FileOutputStream(PanelPrincipal.getDireccion(false)));
                Guardar.writeObject(PanelPrincipal.getTexto().getText());
                Guardar.writeObject(PanelPrincipal.getTexto().getStyledDocument());
                PanelPrincipal.setDireccion(PanelPrincipal.getDireccion(false));
            } catch (Exception E) {
                new VentanaMensajeError("Seleccione un archivo válido para guardar");
            }
        }
    }

    private static class VentanaMensajeError extends JFrame {
        public VentanaMensajeError(String Texto) {
            setTitle("Error");
            setVisible(true);
            setSize(400, 150);
            setResizable(false);
            setLocationRelativeTo(null);
            setAlwaysOnTop(true);
            setLayout(new BorderLayout());

            JPanel PanelMensaje = new JPanel();
            JPanel PanelBoton = new JPanel();
            JLabel Mensaje = new JLabel(Texto);
            JButton Boton = new JButton("Aceptar");

            PanelMensaje.add(Mensaje);
            PanelBoton.add(Boton);
            Boton.addActionListener(new escuchador());
            add(PanelMensaje, BorderLayout.NORTH);
            add(PanelBoton, BorderLayout.SOUTH);
        }

        class escuchador implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        }
    }

}