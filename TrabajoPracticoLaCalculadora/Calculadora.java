package TrabajoPracticoLaCalculadora;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Calculadora extends javax.swing.JFrame {

    ScriptEngineManager SEM = new ScriptEngineManager();
    ScriptEngine SE = SEM.getEngineByName("javascript");
    
    public Calculadora() {
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_2 = new javax.swing.JButton();
        btn_1 = new javax.swing.JButton();
        btn_atras = new javax.swing.JButton();
        btn_resultado = new javax.swing.JButton();
        btn_8 = new javax.swing.JButton();
        btn_5 = new javax.swing.JButton();
        btn_6 = new javax.swing.JButton();
        btn_4 = new javax.swing.JButton();
        btn_9 = new javax.swing.JButton();
        btn_7 = new javax.swing.JButton();
        btn_c = new javax.swing.JButton();
        btn_AbrirParentesis = new javax.swing.JButton();
        btn_CerrarParentesis = new javax.swing.JButton();
        btn_dividir = new javax.swing.JButton();
        btn_multiplicar = new javax.swing.JButton();
        btn_sumar = new javax.swing.JButton();
        btn_3 = new javax.swing.JButton();
        btn_punto = new javax.swing.JButton();
        btn_0 = new javax.swing.JButton();
        btn_restar = new javax.swing.JButton();
        Operacion = new javax.swing.JLabel();
        Resultado = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_2.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_2.setText("2");
        btn_2.setFocusPainted(false);
        btn_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_2.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_2ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 420, 100, 100));

        btn_1.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_1.setText("1");
        btn_1.setFocusPainted(false);
        btn_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_1.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_1ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 420, 100, 100));

        btn_atras.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_atras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_atras.setText("<-");
        btn_atras.setFocusPainted(false);
        btn_atras.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_atras.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_atrasActionPerformed(evt);
            }
        });
        jPanel1.add(btn_atras, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 520, 100, 100));

        btn_resultado.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_resultado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn3.png"))); // NOI18N
        btn_resultado.setText("=");
        btn_resultado.setFocusPainted(false);
        btn_resultado.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_resultado.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn3_presionado.png"))); // NOI18N
        btn_resultado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_resultadoActionPerformed(evt);
            }
        });
        jPanel1.add(btn_resultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 520, 100, 100));

        btn_8.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_8.setText("8");
        btn_8.setFocusPainted(false);
        btn_8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_8.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_8ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_8, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 220, 100, 100));

        btn_5.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_5.setText("5");
        btn_5.setFocusPainted(false);
        btn_5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_5.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_5ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_5, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 320, 100, 100));

        btn_6.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_6.setText("6");
        btn_6.setFocusPainted(false);
        btn_6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_6.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_6ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 320, 100, 100));

        btn_4.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_4.setText("4");
        btn_4.setFocusPainted(false);
        btn_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_4.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_4ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_4, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 320, 100, 100));

        btn_9.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_9.setText("9");
        btn_9.setFocusPainted(false);
        btn_9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_9.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_9ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_9, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 220, 100, 100));

        btn_7.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_7.setText("7");
        btn_7.setFocusPainted(false);
        btn_7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_7.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_7ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 220, 100, 100));

        btn_c.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_c.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2.png"))); // NOI18N
        btn_c.setText("C");
        btn_c.setFocusPainted(false);
        btn_c.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_c.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2_presionado.png"))); // NOI18N
        btn_c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cActionPerformed(evt);
            }
        });
        jPanel1.add(btn_c, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 100, 100));

        btn_AbrirParentesis.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_AbrirParentesis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2.png"))); // NOI18N
        btn_AbrirParentesis.setText("(");
        btn_AbrirParentesis.setFocusPainted(false);
        btn_AbrirParentesis.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_AbrirParentesis.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2_presionado.png"))); // NOI18N
        btn_AbrirParentesis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AbrirParentesisActionPerformed(evt);
            }
        });
        jPanel1.add(btn_AbrirParentesis, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 120, 100, 100));

        btn_CerrarParentesis.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_CerrarParentesis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2.png"))); // NOI18N
        btn_CerrarParentesis.setText(")");
        btn_CerrarParentesis.setFocusPainted(false);
        btn_CerrarParentesis.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_CerrarParentesis.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2_presionado.png"))); // NOI18N
        btn_CerrarParentesis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CerrarParentesisActionPerformed(evt);
            }
        });
        jPanel1.add(btn_CerrarParentesis, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 120, 100, 100));

        btn_dividir.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_dividir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2.png"))); // NOI18N
        btn_dividir.setText("/");
        btn_dividir.setFocusPainted(false);
        btn_dividir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_dividir.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2_presionado.png"))); // NOI18N
        btn_dividir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_dividirActionPerformed(evt);
            }
        });
        jPanel1.add(btn_dividir, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 120, 100, 100));

        btn_multiplicar.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_multiplicar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2.png"))); // NOI18N
        btn_multiplicar.setText("X");
        btn_multiplicar.setFocusPainted(false);
        btn_multiplicar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_multiplicar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2_presionado.png"))); // NOI18N
        btn_multiplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_multiplicarActionPerformed(evt);
            }
        });
        jPanel1.add(btn_multiplicar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 220, 100, 100));

        btn_sumar.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_sumar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2.png"))); // NOI18N
        btn_sumar.setText("+");
        btn_sumar.setFocusPainted(false);
        btn_sumar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_sumar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2_presionado.png"))); // NOI18N
        btn_sumar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sumarActionPerformed(evt);
            }
        });
        jPanel1.add(btn_sumar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 420, 100, 100));

        btn_3.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_3.setText("3");
        btn_3.setFocusPainted(false);
        btn_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_3.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_3ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 420, 100, 100));

        btn_punto.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_punto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_punto.setText(".");
        btn_punto.setFocusPainted(false);
        btn_punto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_punto.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_punto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_puntoActionPerformed(evt);
            }
        });
        jPanel1.add(btn_punto, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 520, 100, 100));

        btn_0.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_0.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1.png"))); // NOI18N
        btn_0.setText("0");
        btn_0.setFocusPainted(false);
        btn_0.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_0.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn1_presionado.png"))); // NOI18N
        btn_0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_0ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_0, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 520, 100, 100));

        btn_restar.setFont(new java.awt.Font("Arial", 0, 50)); // NOI18N
        btn_restar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2.png"))); // NOI18N
        btn_restar.setText("-");
        btn_restar.setFocusPainted(false);
        btn_restar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_restar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/TrabajoPracticoLaCalculadora/btn2_presionado.png"))); // NOI18N
        btn_restar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_restarActionPerformed(evt);
            }
        });
        jPanel1.add(btn_restar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 320, 100, 100));

        Operacion.setFont(new java.awt.Font("Arial Black", 0, 20)); // NOI18N
        Operacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(Operacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 380, 40));

        Resultado.setFont(new java.awt.Font("Arial Black", 0, 50)); // NOI18N
        Resultado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(Resultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 44, 380, 70));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 399, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                        

    private void btn_cActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText("");
        Resultado.setText("");
    }                                     

    private void btn_AbrirParentesisActionPerformed(java.awt.event.ActionEvent evt) {                                                    
        Operacion.setText(Operacion.getText() + '(');
    }                                                   

    private void btn_CerrarParentesisActionPerformed(java.awt.event.ActionEvent evt) {                                                     
        Operacion.setText(Operacion.getText() + ')');
    }                                                    

    private void btn_dividirActionPerformed(java.awt.event.ActionEvent evt) {                                            
        Operacion.setText(Operacion.getText() + '/');
    }                                           

    private void btn_multiplicarActionPerformed(java.awt.event.ActionEvent evt) {                                                
        Operacion.setText(Operacion.getText() + '*');
    }                                               

    private void btn_restarActionPerformed(java.awt.event.ActionEvent evt) {                                           
        Operacion.setText(Operacion.getText() + '-');
    }                                          

    private void btn_sumarActionPerformed(java.awt.event.ActionEvent evt) {                                          
        Operacion.setText(Operacion.getText() + '+');
    }                                         

    private void btn_7ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '7');
    }                                     

    private void btn_8ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '8');
    }                                     

    private void btn_9ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '9');
    }                                     

    private void btn_6ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '6');
    }                                     

    private void btn_5ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '5');
    }                                     

    private void btn_4ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '4');
    }                                     

    private void btn_3ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '3');
    }                                     

    private void btn_2ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '2');
    }                                     

    private void btn_1ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '1');
    }                                     

    private void btn_puntoActionPerformed(java.awt.event.ActionEvent evt) {                                          
        Operacion.setText(Operacion.getText() + '.');
    }                                         

    private void btn_0ActionPerformed(java.awt.event.ActionEvent evt) {                                      
        Operacion.setText(Operacion.getText() + '0');
    }                                     

    private void btn_atrasActionPerformed(java.awt.event.ActionEvent evt) {                                          
        Operacion.setText(Operacion.getText().substring(0, Operacion.getText().length()-1));
    }                                         

    private void btn_resultadoActionPerformed(java.awt.event.ActionEvent evt) {                                              
                try {
                    Resultado.setText(SE.eval(Operacion.getText()).toString());
                } catch(Exception e) {
                    Resultado.setText("ERROR");
                }
    }                                             

    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Calculadora().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JLabel Operacion;
    private javax.swing.JLabel Resultado;
    private javax.swing.JButton btn_0;
    private javax.swing.JButton btn_1;
    private javax.swing.JButton btn_2;
    private javax.swing.JButton btn_3;
    private javax.swing.JButton btn_4;
    private javax.swing.JButton btn_5;
    private javax.swing.JButton btn_6;
    private javax.swing.JButton btn_7;
    private javax.swing.JButton btn_8;
    private javax.swing.JButton btn_9;
    private javax.swing.JButton btn_AbrirParentesis;
    private javax.swing.JButton btn_CerrarParentesis;
    private javax.swing.JButton btn_atras;
    private javax.swing.JButton btn_c;
    private javax.swing.JButton btn_dividir;
    private javax.swing.JButton btn_multiplicar;
    private javax.swing.JButton btn_punto;
    private javax.swing.JButton btn_restar;
    private javax.swing.JButton btn_resultado;
    private javax.swing.JButton btn_sumar;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration                   
}
