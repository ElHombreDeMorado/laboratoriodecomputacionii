package TtrabajoIndividialBanco;

import java.io.Serializable;

public class CuentaCorriente implements Serializable {
    private String NombreTitular;
    private Double Saldo;
    private int NroDeCuenta;

    public CuentaCorriente(String NombreTitular, double SaldoInicial, int NroDeCuenta)
    {
        this.NombreTitular = NombreTitular;
        this.Saldo = SaldoInicial;
        this.NroDeCuenta = NroDeCuenta;
    }

    public void setSaldo(Double saldo) {
        if (this.Saldo + saldo < 0) System.out.println("Saldo insuficiente");
        else this.Saldo += saldo;
    }

    public Double getSaldo() {
        return Saldo;
    }

    public int getNroDeCuenta() {
        return NroDeCuenta;
    }

    @Override
    public String toString() {
        return "CuentaCorriente{" +
                "NombreTitular='" + NombreTitular + '\'' +
                ", Saldo=" + Saldo +
                ", NroDeCuenta=" + NroDeCuenta +
                '}';
    }

    public void TransferirDinero(CuentaCorriente Cuenta2, double saldo)
    {
        if (saldo > 0)
        {
            double ComprobarTransaccion = this.Saldo;
            this.setSaldo(-saldo);
            if (this.Saldo < ComprobarTransaccion) Cuenta2.setSaldo(saldo);
        }
        else System.out.println("Cantidad no válida");
    }
}

