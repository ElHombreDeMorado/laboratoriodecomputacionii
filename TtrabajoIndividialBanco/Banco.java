package TtrabajoIndividialBanco;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Banco {

    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        Set<CuentaCorriente> Cuentas = new HashSet<>();
        boolean Exit = false;

        //Leer el archivo de texto-----------------------------------------------------------------------------
        try {
            ObjectInputStream FlujoEntrada = new ObjectInputStream(new FileInputStream("Banco.dat"));
            Cuentas = (Set<CuentaCorriente>) FlujoEntrada.readObject();
            FlujoEntrada.close();
        } catch (Exception e) {
            System.out.println("Se creará el archivo Banco.dat al finalizar.");
        }
        //Fin lectura------------------------------------------------------------------------------------------

        do {
            System.out.println("\nElija una opción:\n[1]Administrar o crear una cuenta\n[2]Listas cuentas\n[3]Guardar y salir");
            switch ((int) ComprobarDato(scan.next(), true))
            {
                case 1:
                {
                    int NroDeCuenta = IngresarNroDeCuenta();
                    boolean Existe = false;

                    for (CuentaCorriente cuenta: Cuentas) {
                        if (cuenta.getNroDeCuenta() == NroDeCuenta) {
                            do {
                                System.out.println("\nElija una opción:\n[1]Ingresar o sacar dinero\n[2]Ver el saldo de la cuenta");
                                System.out.println("[3]Ver datos de la cuenta\n[4]Transferir dinero a otra cuenta\n[5]Salir");
                                switch ((int) ComprobarDato(scan.next(), true))
                                {
                                    case 1:
                                    {
                                        System.out.println("Cantidad a ingresar o a sacar (positivo=ingresar, negativo=sacar)");
                                        cuenta.setSaldo(ComprobarDato(scan.next(), false)); break;
                                    }
                                    case 2: System.out.println(cuenta.getSaldo()); break;
                                    case 3: System.out.println("\n" + cuenta.toString()); break;
                                    case 4:
                                    {
                                        NroDeCuenta = IngresarNroDeCuenta();

                                        for (CuentaCorriente CuentaReceptora: Cuentas) {
                                            if (CuentaReceptora.getNroDeCuenta() == NroDeCuenta)
                                            {
                                                cuenta.TransferirDinero(CuentaReceptora, ComprobarDato(scan.next(), false));
                                                Existe = true; break;
                                            }
                                        }

                                        if (!Existe) System.out.println("La cuenta no existe"); break;
                                    }
                                    case 5: Exit = true; break;
                                    default: System.out.println("Opción no válida");
                                }
                            } while (!Exit);
                            Exit = false; Existe = true; break;
                        }
                    }

                    if (!Existe)
                    {
                        System.out.println("La cuenta no existe, ¿Desea crearla?(s/n)");
                        char si_no;

                        do {
                            si_no = scan.next().charAt(0);
                        } while (si_no != 's' && si_no != 'n');

                        if (si_no == 's')
                        {
                            System.out.println("Ingrese nombre del titular");
                            String nombre = scan.next();

                            System.out.println("Ingrese saldo inicial");
                            double saldo;
                            do {
                                saldo = ComprobarDato(scan.next(), false);
                                if (saldo < 0) System.out.println("Saldo no válido");
                            } while (saldo < 0);

                            Cuentas.add(new CuentaCorriente(nombre, saldo, NroDeCuenta));
                        }
                    }
                    break;
                }
                case 2: System.out.println("\n" + Cuentas.toString()); break;
                case 3: Exit = true; break;
                default: System.out.println("Opción no válida");
            }
        } while (!Exit);

        //Escribir en el archivo de texto----------------------------------------------------------------------
        try {
            ObjectOutputStream FlujoSalida = new ObjectOutputStream(new FileOutputStream("Banco.dat"));
            FlujoSalida.writeObject(Cuentas);
            FlujoSalida.close();
        } catch (Exception e) {
            System.out.println("No se creó el archivo.");
        }
        //Fin escritura----------------------------------------------------------------------------------------
    }

    public static double ComprobarDato(String dato, boolean entero) {
        try {
            if (entero) return Integer.parseInt(dato);
            else return Double.parseDouble(dato);
        } catch (Exception e) {
            return -0.1;
        }
    }

    public static int IngresarNroDeCuenta()
    {
        System.out.println("Ingrese número de la cuenta");
        int NroDeCuenta;
        do {
            NroDeCuenta = (int) ComprobarDato(scan.next(), true);
            if (NroDeCuenta < 1) System.out.println("Número de cuenta no válido");
        } while (NroDeCuenta < 1);
        return NroDeCuenta;
    }
}

