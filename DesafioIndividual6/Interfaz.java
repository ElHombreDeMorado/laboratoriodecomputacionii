package DesafioIndividual6;

import javax.swing.*;
import java.awt.event.*;

public class Interfaz extends JFrame {
    private JButton Boton1;
    private JButton Boton2;
    private JPanel Panel;

    public Interfaz()
    {
        add(Panel);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        EventoFocoBoton1 foco1 = new EventoFocoBoton1();
        EventoFocoBoton2 foco2 = new EventoFocoBoton2();
        EventoClick click = new EventoClick();

        Boton1.addFocusListener(foco1);
        Boton2.addFocusListener(foco2);
        Panel.addMouseListener(click);
    }

    class EventoFocoBoton1 implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
            System.out.println("En botón 1 ha ganado el foco");
        }

        @Override
        public void focusLost(FocusEvent e) {

        }
    }

    class EventoFocoBoton2 implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
            System.out.println("En botón 2 ha ganado el foco");
        }

        @Override
        public void focusLost(FocusEvent e) {

        }
    }

    class EventoClick extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            super.mousePressed(e);
            System.out.println("Se produjo un evento");
        }
    }
}
